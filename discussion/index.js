let express = require("express");
const { default: mongoose } = require("mongoose");

const taskRoute = require("./routes/routes.js")

const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.connect("mongodb+srv://joshuaniccolo:awesome1@wdc028-course-booking.qwfja.mongodb.net/b170-to-do?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the database"))

app.use("/tasks", taskRoute);



















app.listen(port, () => console.log(`Server is running at port ${port}`));