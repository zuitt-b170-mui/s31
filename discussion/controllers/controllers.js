const task = require("../models/task.js");
const Task = require("../models/task.js");

module.exports.getAllTasks = () => {
    return Task.find({}).then(result =>{
        return result;
    })
}

module.exports.createTask = (requestBody) => {
    let newTask = new Task({
        name: requestBody.name
    })
    return newTask.save().then((savedTask, error) => {
        if(error){
            console.log(error);
            return false;
        }else{
            return savedTask
        }
    })
}

module.exports.deleteTask = (taskId) => {
    return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
        if(error){
            console.log(error);
            return false
        }else{
            return removedTask
        }
    })
}

module.exports.updateTask = ((taskId, requestBody) => {
    return Task.findById(taskId).then((result, error) =>{
        if(error){
            console.log(error)
            return false
        }else{
            result.name = requestBody.name;
            return result.save().then((updateTask, error) =>{
                if(error){
                    console.log(error)
                    return false
                }else{
                    return updateTask
                }
            })
        }
    })
})

module.exports.statusTask = ((taskId, requestBody) => {
    return Task.findById(taskId).then((result, error) =>{
        if(error){
            console.log(error)
            return false
        }else{
            result.name = requestBody.name;
            return result.save().then((statusTask, error) =>{
                if(error){
                    console.log(error)
                    return false
                }else{
                    return statusTask
                }
            })
        }
    })
})