const express = require("express");
const router = express.Router();

const taskController = require("../controllers/controllers.js");
const task = require("../models/task.js");

router.get("/", (req, res) => {
    taskController.getAllTasks().then(result => res.send(result));
});

router.post("/", (req,res) => {
    taskController.createTask(req.body).then(result => res.send(result));
})

router.delete("/:id", (req, res) =>{
    taskController.deleteTask(req.params.id).then(result => res.send(result))
})

router.put("/:id", (req, res)=>{
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})

router.patch("/:id", (req, res)=>{
	taskController.statusTask(req.params.id, req.body).then(result => res.send(result));
})

module.exports = router;